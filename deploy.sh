#! /bin/bash

if [ -z "$1" ];  then
        echo "  [!] No argument supplied, this script expects a docker tag to run"
        exit 1

fi

tag=$1
front_image="pvynych/root-todo_frontend"
back_image="pvynych/root-todo_backend"
nginx_image="pvynych/root-todo_nginx"

echo "[>] Starting deployment"

echo "  [+] Remove containers, volume and networks older than 1 week..."
docker system prune --force --filter "until=168h"

cd docker || return

echo "  [+]  Bitbucket commit ID: $tag"

echo "  [+]  Pull images"
pull=$(docker pull $front_image:$tag)
pull=$(docker pull $back_image:$tag)
pull=$(docker pull $nginx_image:$tag)

if [[ -z "$pull" ]]; then
        echo "  [!] Fail to pull image with tag $tag"
        exit 1
fi

echo "  [+] Start (or Restart) containers: make up (docker-compose up -d)"
TAG=$tag make up
echo "[>] Deployment done."
